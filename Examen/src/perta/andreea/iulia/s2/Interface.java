package perta.andreea.iulia.s2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileReader;
import java.util.Scanner;

public class Interface extends JFrame implements ActionListener {

    private JTextField textField;
    private JButton button;
    private JPanel panel;
    FileReader fileReader;
    private String data;

    public Interface() {
        setContentPane(panel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setSize(1000, 700);
        setVisible(true);
        button.setText("Cilck here!");
        button.addActionListener(this);
        textField.setEditable(false);
    }

    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == button) {
            try {
                fileReader = new FileReader("src/perta/andreea/iulia/s2/Text");
                Scanner myReader = new Scanner(fileReader);
                while (myReader.hasNextLine()) {
                    data = myReader.nextLine();
                }
                myReader.close();
            } catch (Exception ev) {
                JOptionPane.showMessageDialog(null, e + "");
            }

            textField.setText(data);
            StringBuilder sb = new StringBuilder(data);
            System.out.println("Textul inversat este:");
            System.out.println(sb.reverse().toString());

        }
    }

    public static void main(String[] args) {
        Interface i = new Interface();
    }


}
