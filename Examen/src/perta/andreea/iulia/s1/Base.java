package perta.andreea.iulia.s1;

import java.util.ArrayList;

class Base {
    public float n;

    public void f(int g) {
    }
}

class L extends Base {
    long t;
    public M m1;

    public void f() {
    }
}

class X {
    public void i(L l) {
    }
}

class M {

}

class A {
    public M m2;

    public A() {
        m2 = new M();
    }

    public void metA() {
    }
}

class B {
    public ArrayList<M> m3 = new ArrayList<M>();

    public void metB(M m4) {
        m3.add(m4);
    }
}